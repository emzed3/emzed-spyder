param(
    [string]$pythonversion
)

if (-not (Test-Path nuget.exe )) {
    [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
	Invoke-WebRequest https://dist.nuget.org/win-x86-commandline/latest/nuget.exe -Outfile nuget.exe
}
.\nuget.exe install Tools.InnoSetup -Version 6.2.2
.\Tools.InnoSetup.6.2.2/tools/ISCC.exe /Dpythonversion=$pythonversion installer.iss
