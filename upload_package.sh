#! /bin/sh
#
# upload_package.sh
# Copyright (C) 2020 Uwe Schmitt <uwe.schmitt@id.ethz.ch>
#
# Distributed under terms of the MIT license.
#


rm -rf dist
python setup.py sdist
twine upload dist/*
