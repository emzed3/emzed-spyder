#!/bin/bash
#
# build_wheel.sh
# Copyright (C) 2021 Uwe Schmitt <uwe.schmitt@id.ethz.ch>
#
# Distributed under terms of the MIT license.
#

docker build -t build_sip_wheel_ubuntu_20_04 .
docker run -v $(pwd):/tmp build_sip_wheel_ubuntu_20_04
echo
ls -l *.whl
