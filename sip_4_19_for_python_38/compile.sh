#!/bin/bash
#
# compile.sh
# Copyright (C) 2021 Uwe Schmitt <uwe.schmitt@id.ethz.ch>
#
# Distributed under terms of the MIT license.
#

apt update
apt install -y wget python3-dev build-essential

wget https://www.riverbankcomputing.com/static/Downloads/sip/4.19.24/sip-4.19.24.tar.gz
tar xzf sip-4.19.24.tar.gz
cd sip-4.19.24
python3 configure.py
make
