from setuptools import setup, Distribution


class BinaryDistribution(Distribution):
    def has_ext_modules(foo):
        return True


setup(
    name="sip",
    version="4.19.24",
    packages=[""],
    package_dir={"": "."},
    package_data={
        "": ["sip.so"],
    },
    distclass=BinaryDistribution,
)
