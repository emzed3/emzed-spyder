About
=====

Since we depend on some older version of PyQt we also need to install sip<4.2
which is not available for Python 3.8 on anymore.

This folder contains the needed wheel which I created by patching internal meta-
data and by renaming the wheel appropriatly. The wheel contains one shared .so
file which is compatible with Python 3.8.

This package needs to be uploaded to pypi-sissource.ethz.ch such that installers
can find it.

