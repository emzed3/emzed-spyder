# setup:
#   - dependencies are listed in dependencies.txt
#   - pip-compile creates a requirements.txt file from with resolved and
#     conflict free versions
#   - we remove mac specific packages listed in mac_specific_packages.txt
#     in order to create a requirements.txt file which will also work on
#     linux and windows.

OUT=requirements.txt
IN=dependencies.txt
MAC_PACKAGES=mac_specific_packages.txt

pip-compile --quiet --no-header --no-annotate -U -o - ${IN} \
 | grep -v -f ${MAC_PACKAGES} \
 | grep '==' \
 > ${OUT}

echo "please don't forget to commit ${OUT}"
