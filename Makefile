.PHONY: help clean clean-pyc clean-build list test test-all coverage docs release sdist

help:
	@echo
	@echo "check       - check code with flake8"
	@echo "style-check - run tests quickly with the default Python"
	@echo "reformat    - run isort and black"
	@echo "test-all    - run tests on every Python version with tox"
	@echo "coverage    - check code coverage quickly with the default Python"
	@echo "docs        - generate Sphinx HTML documentation, including API docs"
	@echo "sdist       - package"
	@echo "upload_sis  - uploads package to pypi"

clean: clean-build clean-pyc

clean-build:
	rm -fr build/
	rm -fr dist/
	rm -fr *.egg-info

clean-pyc:
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -rf {} +

style-check:
	git ls-tree HEAD -r --name-only\
		| grep '^\(setup\.py\|src\|tests\|benchmarks\)'\
		| grep '\.py$$'\
		| xargs ruff check --line-length=88 --ignore E731,E741

reformat:
	./reformat_code.sh

test:
	py.test tests

test-all:
	tox

coverage:
	coverage run --source emzed.lib -m pytest tests
	coverage report -m
	coverage html
	open htmlcov/index.html

build_docs:
	$(MAKE) -C docs clean
	$(MAKE) -C docs html

docs: build_docs
	open docs/_build/html/index.html

sdist: clean
	python setup.py sdist
	ls -l dist

upload_sis: sdist
	twine upload dist/*
