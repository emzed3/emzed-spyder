#!/usr/bin/env python

import os

import pkg_resources

emzed_spyder_location = pkg_resources.require("emzed_spyder")[0].location
os.environ["EMZED_SPYDER_LOCATION"] = emzed_spyder_location
