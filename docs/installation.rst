How to install and start emzed-spyder
=====================================

We highly recommend to use a virtual environment to install emzed spyder.


.. code-block:: bash

    $ pip install emzed-spyder


After successful installation you can start spyder

.. code-block:: bash

   $ emzed.spyder

.. note::

   During the first startup-up ``emzed.spyder`` will download and install many Python
   packages which can take a while!


You can also download a Windows installer at |emzed-spyder-download-url|.
